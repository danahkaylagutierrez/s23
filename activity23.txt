db.rooms.insertOne({
	"name": "single",
	"accomodates" : 2,
	"price": 1000,
	"description": "A simple room with all basic necessities",
	"rooms_available" : 10,
	"isAvailable" : false,
})

====

db.rooms.insertMany([
	{
	"name": "double",
	"accomodates" : 3,
	"price": 2000,
	"description": "A simple room for a small family going on a vacation",
	"rooms_available" : 5,
	"isAvailable" : false,
	},
{
	"name": "queen",
	"accomodates" : 4,
	"price": 4500,
	"description": "A room with  queen sized bed and all basic necessities needed",
	"rooms_available" : 4,
	"isAvailable" : false,
	},
{
	"name": "deluxe",
	"accomodates" : 8,
	"price": 7000,
	"description": "A room equipped for a group going on a vacation",
	"rooms_available" : 4,
	"isAvailable" : true,
	}
])

=====

db.rooms.find({name:"double"})

=====

db.rooms.updateOne(
{
"_id": ObjectId("61d6cb7503fc69efc94f67b8")
},
{
	$set:{
		"rooms_available" : 0
	}
}

)

====

db.rooms.deleteMany("rooms_available" : 0)





	
